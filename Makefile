défaut:

public/icons-in-terminal.html: bin/navigateur-icons-in-terminal.sh
	mkdir -p $(dir $@)
	$^ > $@
	$(MAKE) $(dir $@)/icons-in-terminal.ttf

public/icons-in-terminal.ttf:
	cp $${ICONS_IN_TERMINAL-icons-in-terminal}/build/icons-in-terminal.ttf $@
